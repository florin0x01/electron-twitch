{
  "targets": [
       {
        "target_name": "obs",
        "sources": ["obs_module/obs.cpp", "obs_module/obs_commander.cpp"],
        "include_dirs": ["<!(node -e \"require('nan')\")", "obs-studio/libobs"],
        "libraries": ['-lobs',
                        '-L../obs-studio/build/libobs',
                       '-Wl,-rpath,./obs-studio/build/libobs',
                       '-Wl,-rpath,./obs-studio/libobs/data'
                       ],
       'cflags!': [ '-fno-exceptions' ],
       'cflags_cc!': [ '-fno-exceptions' ],
       'conditions': [
               ['OS=="mac"', {
                 'xcode_settings': {
                   'GCC_ENABLE_CPP_EXCEPTIONS': 'YES'
                 }
               }]
      ]
    }
  ]
}

