const { app, BrowserWindow } = require('electron')
const assert = require("assert");
const { ipcMain } = require('electron')
const os = require("os")

var windowId


function getNativeWindowHandle_Int(win) {
  let hbuf = win.getNativeWindowHandle()
  if (os.endianness() === "LE") {
    return hbuf.readInt32LE()
  }
  else {
    return hbuf.readInt32BE()
  }
}

function createWindow () {
  // Create the browser window.
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true,
      enableRemoteModule: true
    }
  })

  // and load the index.html of the app.
  // https://stackoverflow.com/questions/55994212/how-use-the-returned-buffer-of-electronjs-function-getnativewindowhandle-i
  windowId = win.id
  win.loadFile('index.html')
  console.log('WindowID ', win.id)
  console.log('HEX ', win.getNativeWindowHandle().toString('hex'))
  win.webContents.on('did-finish-load', () => {
    // win.webContents.send('hwnd', win.getNativeWindowHandle())
    // win.webContents.send('hwnd_id', win.getMediaSourceId())
  })

  // Open the DevTools.
  win.webContents.openDevTools()
}

ipcMain.on('getWindowId', (event, arg) => {
  event.returnValue = windowId
})

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.allowRendererProcessReuse = false
app.whenReady().then(createWindow)

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', () => {
  app.quit()

})

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow()
  }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.

