//
// Created by Florin on 11/10/2020.
//

#include "obs_commander.h"
#include <iostream>
#include <string>
#include <memory>

using namespace std;

ObsException::ObsException(const char *msg): _msg(msg) {}
const char* ObsException::what() const throw() { return _msg; }

ObsCommander::ObsCommander(const string &_plugin_path):plugin_path(_plugin_path) {}
ObsCommander::~ObsCommander() {
   this->shutdown();
}

void ObsCommander::render_graphics(void *param, uint32_t cx, uint32_t cy) {
    std::cout << "Rendering : " << cx << " " << cy << std::endl;
    obs_render_main_texture();
}

obs_module_t *ObsCommander::open_and_init_module(const string &module) {
    obs_module_t *module_ptr = nullptr;
    string path = plugin_path + string("/") + module + string("/") + module + string(".so");
    int status_module = obs_open_module(&module_ptr, path.c_str(), NULL);
    if (status_module != MODULE_SUCCESS) {
        throw ObsException(string("Could not open OBS module " + module + "\n Path : " + path).c_str());
       // return nullptr;
    }

    if (!obs_init_module(module_ptr)) {
        throw ObsException(string("Could not init OBS module " + module + "\n Path : " + path).c_str());
      //  return nullptr;
    }
    return module_ptr;
}

void ObsCommander::init_graphics() {
    ovi.graphics_module = "libobs-opengl";
    ovi.fps_num = 30;
    ovi.fps_den = 1;
    ovi.base_width = 1920;
    ovi.base_height = 1200;
    ovi.output_width = 640;
    ovi.output_height = 480;
    //https://obsproject.com/docs/reference-libobs-media-io.html
    ovi.output_format = VIDEO_FORMAT_RGBA;
    ovi.adapter = 0;
    ovi.gpu_conversion = false;

    //YUV
    ovi.colorspace = VIDEO_CS_DEFAULT;
    ovi.range = VIDEO_RANGE_DEFAULT;

    ovi.scale_type = OBS_SCALE_DISABLE; //https://obsproject.com/docs/reference-sources.html
    int status = obs_reset_video(&ovi);
    if (status != OBS_VIDEO_SUCCESS && status != OBS_VIDEO_CURRENTLY_ACTIVE) {
        throw ObsException("Could not init OBS video");
    }
}

void ObsCommander::init_audio() {
    oai.samples_per_sec = 44100;
    oai.speakers = SPEAKERS_STEREO; //https://obsproject.com/docs/reference-libobs-media-io.html
    if (!obs_reset_audio(&oai)) {
        throw ObsException("Could not init OBS audio");
    }
}

void ObsCommander::start_obs() {
    if (obs_initialized()) {
        cout << "OBS already initialized " << endl;
        return;
    }
    //https://obsproject.com/docs/frontends.html
    bool ret = obs_startup("en-US", NULL, NULL);
    if (!ret) {
        throw ObsException("Could not start OBS");
    }
    //https://obsproject.com/docs/reference-core.html#c.obs_reset_video
    init_graphics();
    init_audio();

    open_and_init_module("mac-capture");
    open_and_init_module("rtmp-services");
    open_and_init_module("obs-x264");
    open_and_init_module("obs-ffmpeg");
    open_and_init_module("obs-outputs");

    //obs_load_all_modules();
    obs_post_load_modules();

    std::cout << "OBS initialized ! " << std::endl;
}

void ObsCommander::shutdown() {
    this->stop_streaming();

//    if (t1) {
//        obs_sceneitem_remove(t1);
//        t1 = nullptr;
//    }
//    if (t2) {
//        obs_sceneitem_remove(t2);
//        t2 = nullptr;
//    }
    int num_remaining = bnum_allocs();

    printf("Remaining allocated obj: %d\n", num_remaining);
    obs_shutdown();
}

void ObsCommander::make_scene() {
//    if (scene) {
//        std::cout << "Scene already created \n";
//        return;
//    }
    this->start_obs();
    std::cout << "make_scene" << std::endl;

    OBSScene scene = obs_scene_create("Default");
    if (!scene) {
        throw ObsException("Could not init scene");
    }

    //display_capture
    // https://obsproject.com/docs/reference-sources.html

    OBSSource screen_source = obs_source_create("display_capture", "Screen Source", NULL, NULL);

    if (!screen_source) {
        throw ObsException("Could not init screen source");
    }

    OBSSource audio_source = obs_source_create("coreaudio_input_capture", "Audio Source", NULL, NULL);
    if (!audio_source) {
        throw ObsException("Could not init audio source");
    }

   obs_scene_add(scene, screen_source);
   obs_scene_add(scene, audio_source);

    // https://obsproject.com/docs/backend-design.html#output-channels
    obs_set_output_source(0, obs_scene_get_source(scene));

    std::cout << "Created everything for sceene" << std::endl;
}

bool ObsCommander::stop_streaming() {
    if (!this->rtmp_output.Get()) {
        std::cout << "No stream to stop" << std::endl;
        return false;
    }
    std::cout << "Stop Streaming .." << std::endl;
    obs_output_stop(rtmp_output);
    obs_output_release(rtmp_output);
    return true;
}

void ObsCommander::make_encoders_and_output() {
    cout << "make_encoders_and_output" << endl;
    if (!h264_encoder) {
        cout << "h264 encoder not init " << endl;
        h264_encoder = obs_video_encoder_create("obs_x264", "Video Encoder", NULL, NULL);
        if (!h264_encoder) {
            throw ObsException("Could not init h264 encoder");
        }
        obs_encoder_set_video(h264_encoder, obs_get_video());
    }

    if (!aac_encoder) {
        cout << "aac encoder not init " << endl;
        aac_encoder = obs_audio_encoder_create("ffmpeg_aac", "Audio Encoder", NULL, 0, NULL); //mixer id channel = 0
        if (!aac_encoder) {
            throw ObsException("Could not init audio AAC encoder");
        }
        obs_encoder_set_audio(aac_encoder, obs_get_audio());
    }

    if (!rtmp_output) {
        std::cout << "Create RTMP output " << std::endl;
        rtmp_output = obs_output_create("rtmp_output", NULL, NULL,NULL);  // https://obsproject.com/docs/reference-outputs.html
        if (!rtmp_output) {
            throw ObsException("Could not init RTMP output");
        }
        obs_output_set_video_encoder(rtmp_output, h264_encoder);
        obs_output_set_audio_encoder(rtmp_output, aac_encoder,0); //audio index - https://obsproject.com/docs/reference-outputs.html#c.obs_output_set_audio_encoder
    }

}

bool ObsCommander::start_streaming(const char* twitch_key) {
    if (!twitch_key) {
        throw ObsException("No twitch key");
    }
    std::cout << "Streaming .." << std::endl;
    this->make_scene();
    this->make_encoders_and_output();

    std::cout << "Create RTMP service " << std::endl;

    OBSData rtmp_settings = obs_data_create();
    obs_data_set_string(rtmp_settings, "service", "twitch");
    obs_data_set_string(rtmp_settings, "key", twitch_key);
    obs_data_set_string(rtmp_settings, "server", "rtmp://live-fra.twitch.tv/app/{stream_key}");

    rtmp_service = obs_service_create("rtmp_common", "RTMP Service Common", rtmp_settings, NULL); //https://obsproject.com/docs/reference-services.html

    // https://source.arknet.ch/fmorgner/dotfiles/-/commit/c36a7bc106a3ef8c9123a8d519824f4a3d6ace50

    std::cout << "Twitch ... " << std::endl;

    obs_output_set_service(rtmp_output, rtmp_service); /* if a stream */
    std::cout << "Starting output" << std::endl;
    obs_output_start(rtmp_output);
    return true;
}

void ObsCommander::preview_display(objc_object *view_obj) {
    this->make_scene();

    struct gs_init_data gs_data = {};
    gs_data.window.view = view_obj;
    gs_data.cx = 640;
    gs_data.cy = 480;
    gs_data.num_backbuffers = 1;
    gs_data.format = GS_RGBA32F;
    gs_data.zsformat = GS_ZS_NONE;

    std::cout << "Before display_create" << std::endl;
    disp = obs_display_create(&gs_data, 0);
    if (!disp) {
        std::cout << "FAIL display" << std::endl;
        throw ObsException("Could not init GS display");
    }
    std::cout << "INIted display " << std::endl;
    obs_display_add_draw_callback(disp, &ObsCommander::render_graphics, NULL);
    obs_display_set_enabled(disp, true);
    std::cout << "Added callback" << std::endl;
}