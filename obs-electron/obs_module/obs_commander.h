//
// Created by Florin on 11/10/2020.
//

#ifndef OBS_ELECTRON_OBS_COMMANDER_H
#define OBS_ELECTRON_OBS_COMMANDER_H

extern "C" {
#include <obs.h>
}
//#include <obs-internal.h>


#include <exception>
#include <string>
#include "obs_local.hpp"

class ObsException : public std::exception {
public:
    ObsException(const char *msg);

    const char *what() const throw();

private:
    const char *_msg;
};

class ObsCommander {
public:
    ObsCommander(const std::string &_plugin_path);

    ~ObsCommander();

    bool start_streaming(const char *twitch_key);

    bool stop_streaming();

    void shutdown();

    void preview_display(objc_object *view_obj);

private:
    static void render_graphics(void *param, uint32_t cx, uint32_t cy);

    void init_graphics();

    void init_audio();

    void start_obs();

    void make_scene();

    void make_encoders_and_output();

    obs_module_t *open_and_init_module(const std::string &module);

    /*
    OBSScene scene;

    OBSSource screen_source;
    OBSSource audio_source;
    OBSSource scene_source;
    */
    OBSService rtmp_service;


    //obs_scene_t* scene;

//    obs_sceneitem_t *t1;
//    obs_sceneitem_t *t2;

    OBSDisplay disp;

    OBSEncoder h264_encoder;
    OBSEncoder aac_encoder;

    OBSOutput rtmp_output;

    struct obs_video_info ovi;
    struct obs_audio_info oai;
    const std::string plugin_path;
};

#endif //OBS_ELECTRON_OBS_COMMANDER_H
