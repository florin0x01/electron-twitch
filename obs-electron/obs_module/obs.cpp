#include <nan.h>
#include <iostream>
#include "obs_commander.h"

ObsCommander commander("./obs-studio/build/plugins");

void OBS_Stream(const Nan::FunctionCallbackInfo <v8::Value> &info) {
    try {
        v8::Local <v8::Object> buffer_obj = info[0].As<v8::Object>();
        Nan::Utf8String utf8_value(info[0]);
        int len = utf8_value.length();
        if (len <= 0) {
            return Nan::ThrowTypeError("arg must be a non-empty string");
        }
        std::string string_copy(*utf8_value, len);
        std::cout << "Got stream key " << string_copy << std::endl;
        bool status = commander.start_streaming(string_copy.c_str());
        if (!status) {
            info.GetReturnValue().Set(-1);
            return;
        }
        info.GetReturnValue().Set(0);
    }catch(ObsException& ex) {
        Nan::ThrowError(ex.what());
        return;
    }
}

void OBS_StopStream(const Nan::FunctionCallbackInfo <v8::Value> &info) {
    try {
        bool status = commander.stop_streaming();
        if (!status) {
            info.GetReturnValue().Set(-1);
            return;
        }
        info.GetReturnValue().Set(0);
    }catch(ObsException& ex) {
        Nan::ThrowError(ex.what());
        return;
    }
}

void OBS_Shutdown(const Nan::FunctionCallbackInfo <v8::Value> &info) {
    commander.shutdown();
    info.GetReturnValue().Set(0);
}

void OBS_Display(const Nan::FunctionCallbackInfo <v8::Value> &info) {
    //https://stackoverflow.com/questions/51446359/can-i-get-the-mainwindowhandle-of-an-electron-window
    //https://github.com/electron/electron/blob/master/docs/api/browser-window.md#wingetnativewindowhandle
    // https://github.com/electron/electron/issues/7460
    // https://github.com/nodejs/node-addon-api/issues/533
    try {
        v8::Local <v8::Object> buffer_obj = info[0].As<v8::Object>();
        unsigned char *buffer_data = (unsigned char *) node::Buffer::Data(buffer_obj);
        std::cout << "BUF DATA: " << std::hex << *buffer_data << std::endl;
        objc_object *view_obj = *reinterpret_cast<objc_object **>(buffer_data);
        commander.preview_display(view_obj);
        info.GetReturnValue().Set(0);
    }catch(ObsException& ex) {
        std::cout << "EXC :" << ex.what() << std::endl;
        return Nan::ThrowError(ex.what());
    }
}


void Init(v8::Local <v8::Object> exports) {
    v8::Local <v8::Context> context = exports->CreationContext();

    exports->Set(context,
                 Nan::New("OBS_Stream").ToLocalChecked(),
                 Nan::New<v8::FunctionTemplate>(OBS_Stream)->GetFunction(context).ToLocalChecked()
    );

    exports->Set(context,
                 Nan::New("OBS_StopStream").ToLocalChecked(),
                 Nan::New<v8::FunctionTemplate>(OBS_StopStream)->GetFunction(context).ToLocalChecked()
    );
    exports->Set(context,
                 Nan::New("OBS_Display").ToLocalChecked(),
                 Nan::New<v8::FunctionTemplate>(OBS_Display)->GetFunction(context).ToLocalChecked()
    );
    exports->Set(context,
                 Nan::New("OBS_Shutdown").ToLocalChecked(),
                 Nan::New<v8::FunctionTemplate>(OBS_Shutdown)->GetFunction(context).ToLocalChecked()
    );
}

NODE_MODULE(obs, Init)


