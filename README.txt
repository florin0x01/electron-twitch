Only for Mac OS
Run full_install.sh - the idea is to have the obs_studio sources inside this dir.
It also creates a data symlink which points to a data dir with the effects folder, a workaround to have the effects folder available on start
After the sources of obs_studio are there you can run ./rebuild.sh each time to rebuild native modules
To run,

cd obs-electron
./start.sh (uses DYLD_LIBRARY_PATH for linker to locate .so libraries)

C++:
There are still some unallocated objects (how do you unload modules?) and my idea was to use the obs.hpp primitives which already have RAII wrappers
and I renamed the file obs_local.hpp and I removed one add_ref method in the constructor, as I saw the sources were having refcount >=1 without this.

The native module uses a C++ "driver" class which talks to the obs library via the RAII primitives 


Of course there are other issues/bugs, but it's great with the limited time (and docs) I had.
